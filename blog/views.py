from django.shortcuts import render
# ~ from django.http import HttpResponse

posts = [
	{
		'author' 		: 'Brother Chiappa',
		'title'			: 'A Butt Story',
		'content'		: 'Once upon a time...',
		'date_posted'	: 'August 27, 2020'
	},
	{
		'author' 		: 'Sister Chiapz',
		'title'			: 'The Butt is Back',
		'content'		: 'Again, once upon a time...',
		'date_posted'	: 'August 28, 2020'
	}
]

def home(request):
	# ~ return HttpResponse('<h1>Blog Home</h1>')
	context = {
		'posts' : posts
	}
	return render(request, 'blog/home.html', context)

def about(request):
	# ~ return HttpResponse('<h1>Blog About</h1>')
	return render(request, 'blog/about.html', { 'title' : 'About' })
